package main

import (
    "github.com/veandco/go-sdl2/sdl"
    //"fmt"
)

var mnbtn1 button
var mnbtn2 button

func ShowMainMenu(renderer *sdl.Renderer) {
    running = true

    RegisterKeypressHandler(func(scancode sdl.Scancode){if scancode == sdl.SCANCODE_ESCAPE {running = false; gameState=0}})
    defer ResetHandlers()

    renderer.SetDrawColor(158, 158, 255, 255)

    i, _ := sdl.LoadBMP("./assets/main_bg.bmp")
    image, _ := renderer.CreateTextureFromSurface(i)
    _, _, width, height, _ := image.Query()
    rect := &sdl.Rect{0, 0, width, height}

    kfStart := createKeyField("start", func(){gameState = 2; running = false}, func(bool){})
    kfReset := createKeyField("reset", func(){levelState = 1}, func(bool){})
    hints = []string{
        "Press f1",
        "\"start\" \"reset\" are keywords. Use f1 to get a new tip.",
        "I see you understand. No go on and start.",
        "Just type start. Alright?",
        "Really? Is this really neccessary?",
        "Ok I am going to shut up now.",
        "",
        "",
        "",
        "...",
        "You got to be kidding me",
        "",
    }

    helpFunc(false)

    for running {
        renderer.Copy(image, nil, rect)
        kfStart.Blit(renderer)
        kfReset.Blit(renderer)
        BlitHelp(renderer)
        renderer.Present()
        renderer.Clear()
        UpdateEvents()
        sdl.Delay(15)
    }

}

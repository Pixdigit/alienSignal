package main

import (
    "github.com/veandco/go-sdl2/sdl"
)

func level4(renderer *sdl.Renderer) {
    running = true
    RegisterKeypressHandler(escapeListener)
    defer ResetHandlers()

    buttonConf := [][]int{
        []int{0, 1, 0},
    }

    answer := "b"
    solution = "The solution is \"" + answer + "\""
    hints = []string{
        "What happens when you add 9 and 1 but only have one digit?",
        "Overflow to the next digit",
        "In binary adding 1 to 1 creates overflow",
    }


    buttons := []button{}
    for i, r := range buttonConf {
        for j, s := range r {
            butt := createDefaultButton(renderer)
            butt.posx = int32(1920 / 2 - ((len(r) * 84 * 2) / 2) + j * 2 * 84 +84)
            butt.posy = int32(1080 / 2 - ((len(buttonConf) * 84 * 2) / 2) + i * 2 * 84 + 84)
            butt.stateInner = s + 1
            butt.stateOuter = s + 1
            buttons = append(buttons, butt)
        }
    }

    key1 := createKeyField(answer, func(){levelState++; running = false}, func(_ bool){})

    for running {
        UpdateEvents()
        renderer.SetDrawColor(158, 158, 255, 255)
        renderer.Clear()

        for _, b := range buttons {
            b.Blit(renderer)
        }

        BlitHelp(renderer)
        key1.Blit(renderer)
        renderer.Present()
        sdl.Delay(15)
    }

}

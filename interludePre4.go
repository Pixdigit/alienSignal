package main

import (
    "github.com/veandco/go-sdl2/sdl"
)


func interludePre4(renderer *sdl.Renderer) {
    defer ResetHandlers()

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()

    image, rect := createTextText(renderer, "4: More than one", HugeFont, sdl.Color{20, 20, 0, 255}, 1920 / 2, 1080 / 2)

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()
    renderer.Copy(image, nil, rect)
    renderer.Present()

    waitForInteraction()

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()
    renderer.Copy(image, nil, rect)
    renderer.Present()
}

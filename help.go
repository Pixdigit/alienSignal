package main

import (
    "github.com/veandco/go-sdl2/sdl"
)


var hints []string
var tries int
var solution string
var helpText string

func init() {
    hints = []string{}
}

func helpFunc(shoudSolution bool) {
    if tries < 0 {
        helpText = ""
        tries++
    } else if tries < len(hints){
        helpText = hints[tries]
        tries++
    } else {
        tries = 0
    }
    if shoudSolution {
        helpText = solution
        tries = -1
    }
}

func BlitHelp(renderer *sdl.Renderer) {
    width, height, _ := smallFont.SizeUTF8(helpText)

    textSurf, _ := smallFont.RenderUTF8Blended(helpText, sdl.Color{20, 20, 0, 255})
    defer textSurf.Free()

    textText, _ := renderer.CreateTextureFromSurface(textSurf)
    defer textText.Destroy()

    renderer.Copy(textText, nil, &sdl.Rect{200, 100, int32(width), int32(height)})
}

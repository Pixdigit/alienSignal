package main

import(
    "github.com/veandco/go-sdl2/sdl"
)


type ClickHandler func(uint8, uint32, int32, int32)
type MotionHandler func(int32, int32, int32, int32)
type ScrollHandler func(int32, int32)
type KeypressHandler func(sdl.Scancode)
type QuitHandler func()

type handler struct {
    clickHandlers []ClickHandler
    motionHandlers []MotionHandler
    scrollHandlers []ScrollHandler
    keypressHandlers []KeypressHandler
    quitHandlers []QuitHandler
}

var handlers handler

func init() {
    ResetHandlers()
}

func ResetHandlers() {
    handlers = handler{
        make([]ClickHandler, 0),
        make([]MotionHandler, 0),
        make([]ScrollHandler, 0),
        make([]KeypressHandler, 0),
        make([]QuitHandler, 0),
    }
    RegisterKeypressHandler(
        func(scancode sdl.Scancode){
            if scancode == sdl.SCANCODE_F1 {
                helpFunc(false)
            }
            if scancode == sdl.SCANCODE_F12 {
                helpFunc(true)
            }
        },
    )
    tries = -1
    helpFunc(false)
}

func UpdateEvents() {
    for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
        switch t := event.(type) {
			case *sdl.QuitEvent:
				for _, h := range handlers.quitHandlers {
                    h()
                }
			case *sdl.MouseMotionEvent:
                for _, h := range handlers.motionHandlers {
                    x, y := backScale(t.X, t.Y)
                    h(x, y, t.XRel, t.YRel)
                }
			case *sdl.MouseButtonEvent:
                for _, h := range handlers.clickHandlers {
                    x, y := backScale(t.X, t.Y)
                    h(t.Button, t.Type, x, y)
                }
			case *sdl.MouseWheelEvent:
                for _, h := range handlers.scrollHandlers {
                    h(t.X, t.Y)
                }
			case *sdl.KeyboardEvent:
                if t.Type == sdl.KEYDOWN {
                    for _, h := range handlers.keypressHandlers {
                        h(t.Keysym.Scancode)
                    }
                }
			default:
                //DO Nothing
		}
    }
}


func RegisterClickHandler(handler ClickHandler){
    handlers.clickHandlers = append(handlers.clickHandlers, handler)
}
func RegisterMotionHandler(handler MotionHandler){
    handlers.motionHandlers = append(handlers.motionHandlers, handler)
}
func RegisterScrollHandler(handler ScrollHandler){
    handlers.scrollHandlers = append(handlers.scrollHandlers, handler)
}
func RegisterKeypressHandler(handler KeypressHandler){
    handlers.keypressHandlers = append(handlers.keypressHandlers, handler)
}
func RegisterQuitHandler(handler QuitHandler){
    handlers.quitHandlers = append(handlers.quitHandlers, handler)
}

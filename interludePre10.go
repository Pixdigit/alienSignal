package main

import (
    "github.com/veandco/go-sdl2/sdl"
)


func interludePre10(renderer *sdl.Renderer) {
    defer ResetHandlers()

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()

    image, rect := createTextText(renderer, "10", HugeFont, sdl.Color{20, 20, 0, 255}, 1920 / 2, 1080 / 2)
    image2, rect2 := createTextText(renderer, "Does this make sense?", HugeFont, sdl.Color{20, 20, 0, 255}, 1920 / 2, 1080 / 2 + 160)

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()
    renderer.Copy(image, nil, rect)
    renderer.Copy(image2, nil, rect2)
    renderer.Present()

    waitForInteraction()
}

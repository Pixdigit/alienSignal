package main

import (
    "github.com/veandco/go-sdl2/sdl"
)


func splash(renderer *sdl.Renderer) {
    defer ResetHandlers()

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()

    i, _ := sdl.LoadBMP("./assets/ggjScreen.bmp")
    defer i.Free()
    image, _ := renderer.CreateTextureFromSurface(i)
    defer image.Destroy()
    _, _, width, height, _ := image.Query()
    rect := &sdl.Rect{0, 0, width, height}

    renderer.Copy(image, nil, rect)
    renderer.Present()
    sdl.Delay(2500)

}

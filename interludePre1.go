package main

import (
    "github.com/veandco/go-sdl2/sdl"
)


func interludePre1(renderer *sdl.Renderer) {
    defer ResetHandlers()

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()

    //i, _ := sdl.LoadBMP("./assets/main_bg.bmp")

    image, rect := createTextText(renderer, "1: Welcome", HugeFont, sdl.Color{20, 20, 0, 255}, 1920 / 2, 1080 / 2)
    image2, rect2 := createTextText(renderer, "Aliens have made first contact. Translate their Signals.", smallFont, sdl.Color{20, 20, 0, 255}, 1920 / 2, 1080 / 2 + 130)

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()
    renderer.Copy(image, nil, rect)
    renderer.Copy(image2, nil, rect2)
    renderer.Present()

    waitForInteraction()

    image, rect = createTextText(renderer, "Grab a pen and paper", HugeFont, sdl.Color{20, 20, 0, 255}, 1920 / 2, 1080 / 2)
    image2, rect2 = createTextText(renderer, "And get your favorite search enigne up and running.", smallFont, sdl.Color{20, 20, 0, 255}, 1920 / 2, 1080 / 2 + 130)
    image3, rect3 := createTextText(renderer, "These levels are genuinely hard. Don't be afraid of using tips.", smallFont, sdl.Color{20, 20, 0, 255}, 1920 / 2, 1080 / 2 + 130 + 100)
    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()
    renderer.Copy(image, nil, rect)
    renderer.Copy(image2, nil, rect2)
    renderer.Copy(image3, nil, rect3)
    renderer.Present()

    waitForInteraction()

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()
    renderer.Copy(image, nil, rect)
    renderer.Copy(image2, nil, rect2)
    renderer.Copy(image3, nil, rect3)
    renderer.Present()
}

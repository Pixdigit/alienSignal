package main

import (
    "github.com/veandco/go-sdl2/sdl"
    "github.com/veandco/go-sdl2/mix"
)


func init() {
}

func playMusic() {

    mix.OpenAudio(44100, mix.DEFAULT_FORMAT, 2, 4096)
    music, _ := mix.LoadWAV("./assets/Podington_Bear_-_Showers.wav")
    music.Play(1, 10000000)

}

func playSfx(filename string) {
    sfx, _ := mix.LoadWAV("./assets/" + filename + ".wav")
    for i := 0; i < 80 ; i++ {
        mix.Volume(1, 128 - i)
        sdl.Delay(7)
    }
    sfx.Play(-1, 0)
    go func(){sdl.Delay(2000); for i := 0; i < 80 ; i++ {mix.Volume(1, 128 - 80 + i); sdl.Delay(7)}}()
}

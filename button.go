package main

import (
    "github.com/veandco/go-sdl2/sdl"
)

type button struct {
    posx, posy int32
    stateInner, stateOuter int
    inner, outer []*sdl.Texture
    alpha uint8
}

func createDefaultButton(renderer *sdl.Renderer) (button){
    tex, _ := renderer.CreateTexture(sdl.PIXELFORMAT_RGBA8888, sdl.TEXTUREACCESS_STATIC, 0, 0)
    inner := []*sdl.Texture{
        tex,
        importImg(renderer, [3]uint8{20, 20, 0})[0],
        importImg(renderer, [3]uint8{120, 96, 49})[0],
    }
    outer := []*sdl.Texture{
        tex,
        importImg(renderer, [3]uint8{20, 20, 0})[1],
        importImg(renderer, [3]uint8{120, 96, 49})[1],
    }
    return button{0, 0, 1, 1, inner, outer, 255}
}

func (b *button) Blit(renderer *sdl.Renderer) {

    if b.stateInner > 0 {
        b.inner[b.stateInner].SetAlphaMod(b.alpha)
        renderer.Copy(b.inner[b.stateInner], nil, &sdl.Rect{b.posx - 42, b.posy - 42, 84, 84})
    }
    if b.stateOuter > 0 {
        b.outer[b.stateOuter].SetAlphaMod(b.alpha)
        renderer.Copy(b.outer[b.stateOuter], nil, &sdl.Rect{b.posx - 42, b.posy - 42, 84, 84})
    }
}

/*func (b *button) fadeIn(renderer *sdl.Renderer) {
    b.alpha = 0

    target := renderer.GetRenderTarget()
    renderer.Clear()

    for i := 0; i < 127; i++ {
        b.alpha = uint8(i * 2)
        renderer.Clear()
        renderer.Copy(target, nil, nil)
        b.Blit(renderer)
        renderer.Present()
        sdl.Delay(10)
    }
    print("fldskjghlkfghskjdghlsjkdfhglkjsdh\n")
    b.alpha = 255
}

func (b *button) fadeOut(renderer *sdl.Renderer) {
    b.alpha = 255

    target := renderer.GetRenderTarget()
    renderer.Clear()

    for i := 0; i < 127; i++ {
        b.alpha = uint8(255 - i * 2)
        renderer.Clear()
        renderer.Copy(target, nil, nil)
        b.Blit(renderer)
        renderer.Present()
        sdl.Delay(10)
    }
    b.alpha = 0
    print("-------------------------------------\n")
}*/

func (b *button) isCLicked(x, y int32) (bool) {
    return b.posx - 42 < x && x < b.posx + 42 && b.posy - 42 < y && y < b.posy + 42
}

func importImg(renderer *sdl.Renderer, color [3]uint8) ([]*sdl.Texture) {
    innerTemplate, _ := sdl.LoadBMP("./assets/innerCircle.bmp")
    outerTemplate, _ := sdl.LoadBMP("./assets/outerCircle.bmp")

    imgDataInner := innerTemplate.Pixels()
    imgDataOuter := outerTemplate.Pixels()

    for pos := 0; pos < innerTemplate.PixelNum() * 4; pos += innerTemplate.BytesPerPixel() {
        imgDataInner[pos + 1] = color[2]
        imgDataInner[pos + 2] = color[1]
        imgDataInner[pos + 3] = color[0]
        imgDataOuter[pos + 1] = color[2]
        imgDataOuter[pos + 2] = color[1]
        imgDataOuter[pos + 3] = color[0]
    }


    i, _ := renderer.CreateTextureFromSurface(innerTemplate)
    o, _ := renderer.CreateTextureFromSurface(outerTemplate)

    return []*sdl.Texture{i, o}
}

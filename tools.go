package main

import(
    "math"
    "github.com/veandco/go-sdl2/sdl"
    "github.com/veandco/go-sdl2/ttf"
)

var running bool
var levelState int // Current Level
var gameState int // 0 - Exit; 1 - toMenu; 2 - inLevel

var xScale float32
var yScale float32

var HugeFont *ttf.Font
var font *ttf.Font
var smallFont *ttf.Font

func init() {
    ttf.Init()
    smallFont, _ = ttf.OpenFont("./assets/josefin-sans/JosefinSans-Regular.ttf", 70)
    font, _ = ttf.OpenFont("./assets/josefin-sans/JosefinSans-Regular.ttf", 120)
    HugeFont, _ = ttf.OpenFont("./assets/josefin-sans/JosefinSans-Regular.ttf", 170)
    running = true
    levelState = 1
    gameState = 1
    Init()
}

func timingFunc(x int) uint8{
    return uint8(255 / (1+math.Exp(6+float64(x)*-0.1)))
}

func min(a, b int) int {
    if a < b {
        return a
    } else {
        return b
    }
}
func max(a, b int) int {
    if a < b {
        return b
    } else {
        return a
    }
}

func shiftLeft(array []string, elem string) []string {
    dstArray := []string{}
    for i := 0; i < len(array) - 1; i++ {
        dstArray = append(dstArray, array[i + 1])
    }
    dstArray = append(dstArray, elem)
    return dstArray
}

func escapeListener(scancode sdl.Scancode) {
    if scancode == sdl.SCANCODE_ESCAPE {
        running = false
        gameState = 1
    }
}

func waitForInteraction() {
    running := true
    RegisterKeypressHandler(func(sc sdl.Scancode){if sc == sdl.SCANCODE_RETURN{running = false}; if sc == sdl.SCANCODE_ESCAPE{gameState = 1}})
    for running {
        UpdateEvents()
    }
}

func createTextText(renderer *sdl.Renderer, txt string, font *ttf.Font, color sdl.Color, posx, posy int32) (*sdl.Texture, *sdl.Rect) {
    i, _ := font.RenderUTF8Blended(txt, color)
    defer i.Free()
    image, _ := renderer.CreateTextureFromSurface(i)
    _, _, width, height, _ := image.Query()
    rect := &sdl.Rect{posx - width / 2, posy - height / 2, width, height}
    return image, rect
}

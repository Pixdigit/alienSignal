package main

import (
    "github.com/veandco/go-sdl2/sdl"
)

var version string

func main() {

    sdl.Init(sdl.INIT_EVERYTHING)
    defer sdl.Quit()

    playMusic()

	var windowFlags uint32 = uint32(sdl.WINDOW_SHOWN) | uint32(sdl.WINDOW_FULLSCREEN_DESKTOP)
    window, _ := sdl.CreateWindow("AlienSignal", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED, 0, 0, windowFlags)
    windowSizeX, windowSizeY := window.GetSize()

	renderer, _ := sdl.CreateRenderer(window, -1, sdl.RENDERER_PRESENTVSYNC)
    sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "2");
    xScale = float32(windowSizeX) / 1920
    yScale = float32(windowSizeY) / 1080
    renderer.SetScale(xScale, yScale)
    splash(renderer)

    for gameState > 0 {
        if gameState == 1 {
            ShowMainMenu(renderer)
        } else {
            switch levelState {
            case 1:
                interludePre1(renderer)
                level1(renderer)
                playSfx("success")
            case 2:
                interludePre2(renderer)
                level2(renderer)
                playSfx("success")
            case 3:
                interludePre3(renderer)
                level3(renderer)
                playSfx("success")
            case 4:
                interludePre4(renderer)
                level4(renderer)
                playSfx("success")
            case 5:
                interludePre5(renderer)
                level5(renderer)
                playSfx("success")
            case 6:
                interludePre6(renderer)
                level6(renderer)
                playSfx("MacStartUp")
                interludePost6(renderer)
            case 7:
                interludePre7(renderer)
                level7(renderer)
                playSfx("success")
            case 8:
                interludePre8(renderer)
                level8(renderer)
                playSfx("success")
            case 9:
                interludePre9(renderer)
                level9(renderer)
                playSfx("success")
            case 10:
                interludePre10(renderer)
                level10(renderer)
                playSfx("MacStartUp")
                interludePost10(renderer)
            default:
                gameState = 0
            }
        }
    }
}

func backScale(x, y int32)(int32, int32) {
    return int32(float32(x) * (1 / xScale) ), int32(float32(y) * (1 / yScale))
}

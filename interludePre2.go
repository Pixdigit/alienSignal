package main

import (
    "github.com/veandco/go-sdl2/sdl"
)


func interludePre2(renderer *sdl.Renderer) {
    defer ResetHandlers()

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()

    //i, _ := sdl.LoadBMP("./assets/main_bg.bmp")

    image, rect := createTextText(renderer, "2: Extrapolate", HugeFont, sdl.Color{20, 20, 0, 255}, 1920 / 2, 1080 / 2)
    image2, rect2 := createTextText(renderer, "A new element is introduced", smallFont, sdl.Color{20, 20, 0, 255}, 1920 / 2, 1080 / 2 + 130)

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()
    renderer.Copy(image, nil, rect)
    renderer.Copy(image2, nil, rect2)
    renderer.Present()

    waitForInteraction()

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()
    renderer.Copy(image, nil, rect)
    renderer.Copy(image2, nil, rect2)
    renderer.Present()
}

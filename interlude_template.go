package main

import (
    "github.com/veandco/go-sdl2/sdl"
)


func Interlude_(renderer *sdl.Renderer) {
    defer ResetHandlers()

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()

    //i, _ := sdl.LoadBMP("./assets/main_bg.bmp")
    i, _ := HugeFont.RenderUTF8Blended("", sdl.Color{20, 20, 0, 255})
    defer i.Free()
    image, _ := renderer.CreateTextureFromSurface(i)
    defer image.Destroy()
    _, _, width, height, _ := image.Query()
    rect := &sdl.Rect{1920 / 2 - width / 2, 1080 / 2 - height / 2, width, height}

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()
    renderer.Copy(image, nil, rect)
    renderer.Present()

    waitForInteraction()

    renderer.SetDrawColor(158, 158, 255, 255)
    renderer.Clear()
    renderer.Copy(image, nil, rect)
    renderer.Present()
}
